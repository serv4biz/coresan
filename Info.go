package coresan

import (
	"errors"
	"math/rand"
	"time"

	"gitlab.com/serv4biz/gfp/jsons"

	"gitlab.com/serv4biz/coresan/drivers"
)

// Info is get info file from coresan
func (me *CORESAN) Info(txtCSNID string) (*jsons.Object, error) {
	me.MutexMapDataItem.RLock()
	dataItem, itemOk := me.MapDataItem[txtCSNID]
	me.MutexMapDataItem.RUnlock()

	if itemOk {
		dataNodeItem := dataItem.DataNode
		jsoReq := drivers.Info(dataNodeItem.JSOHost, txtCSNID)
		if jsoReq.Int("int_status") > 0 {
			return jsoReq.Object("jso_data").PutString("txt_node", dataNodeItem.Name), nil
		}

		// Update memory
		me.MutexMapDataItem.Lock()
		delete(me.MapDataItem, txtCSNID)
		me.MutexMapDataItem.Unlock()
	}

	// if not found in dataitem
	me.MutexMapDataNode.RLock()
	jsaNodeKey := jsons.ArrayNew(0)
	for key := range me.MapDataNode {
		jsaNodeKey.PutString(key)
	}
	me.MutexMapDataNode.RUnlock()

	rand.New(rand.NewSource(time.Now().UnixNano()))
	for jsaNodeKey.Length() > 0 {
		index := rand.Intn(jsaNodeKey.Length())
		nodeName := jsaNodeKey.String(index)
		jsaNodeKey.Delete(index)

		me.MutexMapDataNode.RLock()
		dataNodeItem := me.MapDataNode[nodeName]
		me.MutexMapDataNode.RUnlock()

		jsoReq := drivers.Info(dataNodeItem.JSOHost, txtCSNID)
		if jsoReq.Int("int_status") > 0 {
			// Update memory
			dataItem = new(DataItem)
			dataItem.DataNode = dataNodeItem
			dataItem.CSNID = txtCSNID

			me.MutexMapDataItem.Lock()
			me.MapDataItem[txtCSNID] = dataItem
			me.MutexMapDataItem.Unlock()

			return jsoReq.Object("jso_data").PutString("txt_node", dataNodeItem.Name), nil
		}
	}

	return nil, errors.New("not found file")
}
