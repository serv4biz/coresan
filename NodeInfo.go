package coresan

import (
	"errors"

	"gitlab.com/serv4biz/coresan/networks"
	"gitlab.com/serv4biz/gfp/jsons"
)

// DataNodeInfo is list all datanode
func (me *CORESAN) NodeInfo(nodeName string) (*jsons.Object, error) {
	jsoCmd := jsons.ObjectNew(0)
	jsoCmd.PutObject("jso_authen", me.AuthenInfo())
	jsoCmd.PutString("txt_command", "node_info")
	jsoCmd.PutString("txt_name", nodeName)

	jsoReq := networks.Request(me.JSOConfigHost, jsoCmd)
	if jsoReq.Int("int_status") > 0 {
		return jsoReq.Object("jso_data"), nil
	}
	return nil, errors.New(jsoReq.String("txt_msg"))
}
