package coresan

import (
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
)

// AuthenInfo is get authentication information
func (me *CORESAN) AuthenInfo() *jsons.Object {
	jsoAuthen := jsons.ObjectNew(0)
	jsoAuthen.PutString("txt_username", strings.TrimSpace(strings.ToLower(me.JSOConfigHost.String("txt_username"))))
	jsoAuthen.PutString("txt_password", strings.TrimSpace(me.JSOConfigHost.String("txt_password")))
	return jsoAuthen
}
