package drivers

import (
	"encoding/base64"
	"strings"

	"gitlab.com/serv4biz/coresan/networks"
	"gitlab.com/serv4biz/gfp/jsons"
)

// Rewrite is rewrite file to coresan
func Rewrite(jsoHost *jsons.Object, txtCSNID string, txtFilename string, intExpire int, buffer []byte) *jsons.Object {
	jsoCmd := jsons.ObjectNew(0)
	jsoCmd.PutString("txt_command", "storage_rewrite")
	jsoCmd.PutString("txt_csnid", strings.TrimSpace(txtCSNID))
	jsoCmd.PutString("txt_filename", txtFilename)
	jsoCmd.PutInt("int_expire", intExpire)
	jsoCmd.PutString("txt_data", base64.StdEncoding.EncodeToString(buffer))

	jsoAuthen := jsons.ObjectNew(0)
	jsoAuthen.PutString("txt_username", strings.TrimSpace(strings.ToLower(jsoHost.String("txt_username"))))
	jsoAuthen.PutString("txt_password", strings.TrimSpace(jsoHost.String("txt_password")))
	jsoCmd.PutObject("jso_authen", jsoAuthen)

	return networks.Request(jsoHost, jsoCmd)
}
