package drivers

import (
	"strings"

	"gitlab.com/serv4biz/coresan/networks"
	"gitlab.com/serv4biz/gfp/jsons"
)

// Unlink is delete file from coresan
func Unlink(jsoHost *jsons.Object, txtCSNID string) *jsons.Object {
	jsoCmd := jsons.ObjectNew(0)
	jsoCmd.PutString("txt_command", "storage_unlink")
	jsoCmd.PutString("txt_csnid", strings.TrimSpace(txtCSNID))

	jsoAuthen := jsons.ObjectNew(0)
	jsoAuthen.PutString("txt_username", strings.TrimSpace(strings.ToLower(jsoHost.String("txt_username"))))
	jsoAuthen.PutString("txt_password", strings.TrimSpace(jsoHost.String("txt_password")))
	jsoCmd.PutObject("jso_authen", jsoAuthen)

	return networks.Request(jsoHost, jsoCmd)
}
