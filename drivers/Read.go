package drivers

import (
	"encoding/base64"
	"strings"

	"gitlab.com/serv4biz/coresan/networks"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
)

// Read is read file from coresan
func Read(jsoHost *jsons.Object, txtCSNID string) (*jsons.Object, []byte) {
	jsoCmd := jsons.ObjectNew(0)
	jsoCmd.PutString("txt_command", "storage_read")
	jsoCmd.PutString("txt_csnid", strings.TrimSpace(txtCSNID))

	jsoAuthen := jsons.ObjectNew(0)
	jsoAuthen.PutString("txt_username", strings.TrimSpace(strings.ToLower(jsoHost.String("txt_username"))))
	jsoAuthen.PutString("txt_password", strings.TrimSpace(jsoHost.String("txt_password")))
	jsoCmd.PutObject("jso_authen", jsoAuthen)

	jsoReq := networks.Request(jsoHost, jsoCmd)
	if jsoReq.Int("int_status") > 0 {
		jsoData := jsoReq.Object("jso_data")
		buffer, err := base64.StdEncoding.DecodeString(jsoData.String("txt_data"))
		if err != nil {
			jsoResult := jsons.ObjectNew(0)
			jsoResult.PutInt("int_status", 0)
			jsoResult.PutString("txt_msg", logs.Message(err))
			return jsoResult, nil
		}

		jsoData.Delete("txt_data")
		return jsoReq, buffer
	}

	return jsoReq, nil
}
