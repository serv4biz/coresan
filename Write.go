package coresan

import (
	"errors"
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/serv4biz/gfp/jsons"

	"gitlab.com/serv4biz/coresan/drivers"
)

// Write is write file to coresan
func (me *CORESAN) Write(txtFilename string, intExpire int, buffer []byte) (*jsons.Object, error) {
	me.MutexMapDataNode.RLock()
	jsaNodeKey := jsons.ArrayNew(0)
	for key := range me.MapDataNode {
		jsaNodeKey.PutString(key)
	}
	me.MutexMapDataNode.RUnlock()

	// If not found then insert row
	rand.New(rand.NewSource(time.Now().UnixNano()))
	me.MutexMapDataNode.RLock()
	dataNodeItem := me.MapDataNode[jsaNodeKey.String(rand.Intn(jsaNodeKey.Length()))]
	me.MutexMapDataNode.RUnlock()

	jsoReq := drivers.Write(dataNodeItem.JSOHost, txtFilename, intExpire, buffer)
	if jsoReq.Int("int_status") == 0 {
		return nil, errors.New(fmt.Sprint("can't write to coresan in node ", dataNodeItem.Name, " [ ", jsoReq.String("txt_msg"), " ]"))
	}

	// Update memory
	jsoData := jsoReq.Object("jso_data")

	dataItem := new(DataItem)
	dataItem.DataNode = dataNodeItem
	dataItem.CSNID = jsoData.String("txt_csnid")

	me.MutexMapDataItem.Lock()
	me.MapDataItem[dataItem.CSNID] = dataItem
	me.MutexMapDataItem.Unlock()
	return jsoData, nil
}
