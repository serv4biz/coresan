package coresan

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/serv4biz/coresan/networks"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/ulids"
)

// Factory is begin HScaleDB object
func Factory(jsoConfigHost *jsons.Object) (*CORESAN, error) {
	myULID, err := ulids.New()
	if err != nil {
		return nil, err
	}

	csnItem := new(CORESAN)
	csnItem.UID = myULID.String()
	csnItem.JSOConfigHost = jsoConfigHost
	csnItem.MapDataNode = make(map[string]*DataNode)
	csnItem.MapDataItem = make(map[string]*DataItem)
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println("CoreSAN Cluster Factory")
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println(fmt.Sprint("UID : ", csnItem.UID))
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println("Loading datanode info.")
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")

	// Get Data node info
	jsaDataNode, errDataNode := csnItem.NodeList()
	if errDataNode != nil {
		return nil, errDataNode
	}

	err = jsaDataNode.EachString(func(index int, value string) error {
		buff := fmt.Sprint(index+1, " ) ", value)
		fmt.Println(buff)

		jsoNodeInfo, err := csnItem.NodeInfo(value)
		if err != nil {
			return err
		}

		nNodeItem := new(DataNode)
		nNodeItem.CORESAN = csnItem
		nNodeItem.Name = strings.ToLower(strings.TrimSpace(jsoNodeInfo.String("txt_name")))
		nNodeItem.JSOHost = jsoNodeInfo.Object("jso_coresan")

		jsoReq := networks.Ping(nNodeItem.JSOHost)
		if jsoReq.Int("int_status") == 0 {
			return errors.New("can't ping network")
		}

		csnItem.MutexMapDataNode.Lock()
		csnItem.MapDataNode[nNodeItem.Name] = nNodeItem
		csnItem.MutexMapDataNode.Unlock()
		return nil
	})
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	if err != nil {
		return nil, err
	}

	return csnItem, nil
}
