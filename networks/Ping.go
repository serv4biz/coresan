package networks

import (
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
)

// Ping is test network
func Ping(jsoHost *jsons.Object) *jsons.Object {
	jsoAuthen := jsons.ObjectNew(0)
	jsoAuthen.PutString("txt_username", strings.TrimSpace(strings.ToLower(jsoHost.String("txt_username"))))
	jsoAuthen.PutString("txt_password", strings.TrimSpace(jsoHost.String("txt_password")))

	jsoCmd := jsons.ObjectNew(0)
	jsoCmd.PutString("txt_command", "network_ping")
	jsoCmd.PutObject("jso_authen", jsoAuthen)
	return Request(jsoHost, jsoCmd)
}
