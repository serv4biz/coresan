package networks

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"time"

	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
)

// Request is command send package to host
func Request(jsoHost, jsoCmd *jsons.Object) *jsons.Object {
	jsoRequest := jsons.ObjectNew(0)
	jsoRequest.PutInt("int_status", 0)

	txtProtocol := jsoHost.String("txt_protocol")
	txtHost := jsoHost.String("txt_host")
	intPort := jsoHost.Int("int_port")

	txtCmd, err := jsoCmd.ToString()
	if logs.Error(err, true) {
		jsoRequest.PutString("txt_msg", logs.Message(err))
		return jsoRequest
	}

	url := fmt.Sprint(txtProtocol, "://", txtHost, ":", intPort, "/")
	req, err := http.NewRequest("POST", url, bytes.NewBuffer([]byte(txtCmd)))
	if err != nil {
		jsoRequest.PutString("txt_msg", logs.Message(err))
		return jsoRequest
	}

	req.Header.Set("Content-Type", "application/json")
	defer req.Body.Close()

	intTime := jsoHost.Int("int_timeout")
	if intTime <= 0 {
		intTime = 15
	}

	timeout := time.Duration(time.Duration(intTime) * time.Second)
	client := &http.Client{Timeout: timeout}
	resp, err := client.Do(req)
	if err != nil {
		jsoRequest.PutString("txt_msg", logs.Message(err))
		return jsoRequest
	}
	defer resp.Body.Close()

	if resp.StatusCode == 200 {
		body, errBody := io.ReadAll(resp.Body)
		if errBody != nil {
			jsoRequest.PutString("txt_msg", logs.Message(err))
			return jsoRequest
		}

		jsoResult, errResult := jsons.ObjectFromString(string(body))
		if errResult != nil {
			jsoRequest.PutString("txt_msg", logs.Message(err))
			return jsoRequest
		}
		return jsoResult
	}

	jsoRequest.PutString("txt_msg", fmt.Sprint("response is not successfully status error code ", resp.StatusCode))
	return jsoRequest
}
