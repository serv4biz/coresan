package coresan

import (
	"sync"

	"gitlab.com/serv4biz/gfp/jsons"
)

// DataNode is struct for datanode info
type DataNode struct {
	sync.RWMutex
	CORESAN *CORESAN

	Name    string
	JSOHost *jsons.Object
}

// DataItem is struct in Cabinet object
type DataItem struct {
	DataNode *DataNode
	CSNID    string
}

// CORESAN is main object
type CORESAN struct {
	sync.RWMutex

	UID           string
	JSOConfigHost *jsons.Object

	MutexMapDataNode sync.RWMutex
	MapDataNode      map[string]*DataNode

	MutexMapDataItem sync.RWMutex
	MapDataItem      map[string]*DataItem
}
