package coresan

import (
	"errors"
	"fmt"

	"gitlab.com/serv4biz/gfp/jsons"

	"gitlab.com/serv4biz/coresan/drivers"
)

// Rewrite is rewrite file to coresan
func (me *CORESAN) Rewrite(txtCSNID string, txtFilename string, intExpire int, buffer []byte) (*jsons.Object, error) {
	jsoInfo, errInfo := me.Info(txtCSNID)
	if errInfo != nil {
		return nil, errInfo
	}

	me.MutexMapDataNode.RLock()
	dataNodeItem := me.MapDataNode[jsoInfo.String("txt_node")]
	me.MutexMapDataNode.RUnlock()

	jsoReq := drivers.Rewrite(dataNodeItem.JSOHost, txtCSNID, txtFilename, intExpire, buffer)
	if jsoReq.Int("int_status") == 0 {
		return nil, errors.New(fmt.Sprint("can't write to coresan in node ", dataNodeItem.Name, " [ ", jsoReq.String("txt_msg"), " ]"))
	}

	// Update memory
	jsoData := jsoReq.Object("jso_data")

	dataItem := new(DataItem)
	dataItem.DataNode = dataNodeItem
	dataItem.CSNID = jsoData.String("txt_csnid")

	me.MutexMapDataItem.Lock()
	me.MapDataItem[dataItem.CSNID] = dataItem
	me.MutexMapDataItem.Unlock()

	return jsoData, nil
}
