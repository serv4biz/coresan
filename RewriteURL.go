package coresan

import (
	"errors"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/serv4biz/gfp/jsons"
)

// Rewrite is rewrite file to coresan
func (me *CORESAN) RewriteURL(txtCSNID string, txtFilename string, intExpire int, txtURL string) (*jsons.Object, error) {
	resp, err := http.Get(txtURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == 200 {
		body, errBody := io.ReadAll(resp.Body)
		if errBody != nil {
			return nil, err
		}
		return me.Rewrite(txtCSNID, txtFilename, intExpire, body)
	}
	return nil, errors.New(fmt.Sprint("Response is not successfully status error code ", resp.StatusCode))
}
