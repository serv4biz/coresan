package main

import (
	"fmt"
	"io"
	"net/http"
	"strings"

	"gitlab.com/serv4biz/coresan/apps/config/global"
	"gitlab.com/serv4biz/gfp/jsons"

	command_network "gitlab.com/serv4biz/coresan/apps/config/commands/network"
	command_node "gitlab.com/serv4biz/coresan/apps/config/commands/node"
)

// WorkHandler is main job for any request
func WorkHandler(w http.ResponseWriter, r *http.Request) {
	r.Body = http.MaxBytesReader(w, r.Body, int64(global.MaxRead))
	defer r.Body.Close()

	w.Header().Set("Content-Type", "application/json")
	jsoResult := jsons.ObjectNew(0)
	jsoResult.PutInt("int_status", 0)

	buffer, errBody := io.ReadAll(r.Body)
	if errBody != nil {
		jsoResult.PutString("txt_msg", fmt.Sprint("can't read body from http request [ ", errBody, " ]"))
	} else {
		jsoCmd, errCmd := jsons.ObjectFromString(string(buffer))
		if errCmd != nil {
			jsoResult.PutString("txt_msg", fmt.Sprint("can't load command from json string buffer [ ", errCmd, " ]"))
		} else {
			jsoAuthen := jsoCmd.Object("jso_authen")
			authenUser := strings.TrimSpace(strings.ToLower(jsoAuthen.String("txt_username")))
			authenPass := strings.TrimSpace(jsoAuthen.String("txt_password"))

			if len(authenUser) > 0 {
				if global.Username == authenUser && global.Password == authenPass {
					switch jsoCmd.String("txt_command") {
					case "network_ping":
						jsoResult = command_network.Ping(jsoCmd)
					case "node_info":
						jsoResult = command_node.Info(jsoCmd)
					case "node_list":
						jsoResult = command_node.List(jsoCmd)
					}
				}
			}
		}
	}

	buffer, err := jsoResult.ToBytes()
	if err != nil {
		jsoData := jsons.ObjectNew(0)
		jsoData.PutInt("int_status", 0)
		jsoData.PutString("txt_msg", fmt.Sprint(err))
		buffer, err := jsoData.ToBytes()
		if err == nil {
			w.Write(buffer)
		}
	} else {
		w.Write(buffer)
	}
}
