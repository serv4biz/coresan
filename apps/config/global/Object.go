package global

import (
	"sync"

	"gitlab.com/serv4biz/gfp/jsons"
)

// AppName is application name
var AppName = "CORESAN CONFIG"

// AppVersion is application version
var AppVersion = "1.0.0"

// CompanyName is name of company
var CompanyName = "SERV4BIZ CO.,LTD."

var AppDir = "./"

// DS is split for directory in path string
var DS = "/"

// MutexJSOConfig is mutex lock for JSOConfig object
var MutexJSOConfig sync.RWMutex

// JSOConfig is json object for application config file
var JSOConfig = jsons.ObjectNew(0)

// Username is authen username
var Username = ""

// Password is authen password
var Password = ""

// MaxRead is Max MultiPart of body in request
var MaxRead int = 0
