package network

import (
	"gitlab.com/serv4biz/gfp/jsons"
)

// Ping is command check network status
func Ping(jsoCmd *jsons.Object) *jsons.Object {
	jsoResult := jsons.ObjectNew(0)
	jsoResult.PutInt("int_status", 1)
	return jsoResult
}
