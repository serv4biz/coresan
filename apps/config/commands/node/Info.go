package node

import (
	"strings"

	"gitlab.com/serv4biz/coresan/apps/config/locals"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
)

// Info is get info of datanode
func Info(jsoCmd *jsons.Object) *jsons.Object {
	jsoResult := jsons.ObjectNew(0)
	jsoResult.PutInt("int_status", 0)

	nodeName := strings.TrimSpace(strings.ToLower(jsoCmd.String("txt_name")))
	nodeInfo, err := locals.LoadNodeInfo(nodeName)
	if err != nil {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return jsoResult
	}

	jsoResult.PutObject("jso_data", nodeInfo)
	jsoResult.PutInt("int_status", 1)
	return jsoResult
}
