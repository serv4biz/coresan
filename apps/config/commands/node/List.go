package node

import (
	"gitlab.com/serv4biz/coresan/apps/config/locals"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
)

// List is list all datanode
func List(jsoCmd *jsons.Object) *jsons.Object {
	jsoResult := jsons.ObjectNew(0)
	jsoResult.PutInt("int_status", 0)

	jsaListing, err := locals.ListNode()
	if err != nil {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return jsoResult
	}

	jsoData := jsons.ObjectNew(0)
	jsoData.PutInt("int_length", jsaListing.Length())
	jsoData.PutArray("jsa_list", jsaListing)
	jsoResult.PutObject("jso_data", jsoData)
	jsoResult.PutInt("int_status", 1)
	return jsoResult
}
