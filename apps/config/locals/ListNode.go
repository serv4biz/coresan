package locals

import (
	"fmt"
	"strings"

	"gitlab.com/serv4biz/coresan/apps/config/global"
	"gitlab.com/serv4biz/coresan/apps/config/utility"
	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/jsons"
)

// ListNode is listing data node
func ListNode() (*jsons.Array, error) {
	jsaList := jsons.ArrayNew(0)
	pathdir := fmt.Sprint(global.AppDir, utility.DS, "nodes")
	filelist, err := files.ScanDir(pathdir)
	if err != nil {
		return nil, err
	}

	for _, fileName := range filelist {
		if strings.HasSuffix(fileName, ".json") {
			jsaList.PutString(strings.TrimSpace(strings.ToLower(strings.TrimSuffix(fileName, ".json"))))
		}
	}
	return jsaList, err
}
