package locals

import (
	"errors"
	"fmt"

	"gitlab.com/serv4biz/coresan/apps/config/global"
	"gitlab.com/serv4biz/coresan/apps/config/utility"
	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/jsons"
)

// LoadConfig is load config.json to json object
func LoadConfig() (*jsons.Object, error) {
	pathfile := fmt.Sprint(global.AppDir, utility.DS, "config.json")
	jsoConfig := jsons.ObjectNew(0)
	jsoConfig.PutString("txt_host", "localhost")
	jsoConfig.PutInt("int_port", 5678)

	if files.ExistFile(pathfile) {
		return jsons.ObjectFromFile(pathfile)

	}
	return jsoConfig, errors.New("not found config.json file")
}
