package locals

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/serv4biz/coresan/apps/config/global"
	"gitlab.com/serv4biz/coresan/apps/config/utility"
	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/jsons"
)

// LoadNodeInfo is load data node info
func LoadNodeInfo(name string) (*jsons.Object, error) {
	pathfile := fmt.Sprint(global.AppDir, utility.DS, "nodes", utility.DS, strings.TrimSpace(strings.ToLower(name)), ".json")
	if files.ExistFile(pathfile) {
		return jsons.ObjectFromFile(pathfile)
	}
	return nil, errors.New(fmt.Sprint("not found ", pathfile, " file"))
}
