package main

import (
	"fmt"
	"io"
	"net/http"
	"strings"

	"gitlab.com/serv4biz/coresan/apps/server/commands/network"
	"gitlab.com/serv4biz/coresan/apps/server/commands/storage"
	"gitlab.com/serv4biz/coresan/apps/server/global"
	"gitlab.com/serv4biz/gfp/jsons"
)

// WorkHandler is main handler all command
func WorkHandler(w http.ResponseWriter, r *http.Request) {
	// Max memory 1024MB
	r.Body = http.MaxBytesReader(w, r.Body, int64(global.MaxRead))
	defer r.Body.Close()

	w.Header().Set("Content-Type", "application/json")
	jsoResult := jsons.ObjectNew(0)
	jsoResult.PutNumber("int_status", 0)

	buffer, errBody := io.ReadAll(r.Body)
	if errBody != nil {
		jsoResult.PutString("txt_msg", fmt.Sprint("can't read body from http request [ ", errBody, " ]"))
	} else {
		jsoCmd, errCmd := jsons.ObjectFromString(string(buffer))
		if errCmd != nil {
			jsoResult.PutString("txt_msg", fmt.Sprint("can't load command from json string buffer [ ", errCmd, " ]"))
		} else {
			jsoAuthen := jsoCmd.Object("jso_authen")
			authenUser := strings.TrimSpace(strings.ToLower(jsoAuthen.String("txt_username")))
			authenPass := strings.TrimSpace(jsoAuthen.String("txt_password"))

			if len(authenUser) > 0 {
				if global.Username == authenUser && global.Password == authenPass {
					switch jsoCmd.String("txt_command") {
					case "network_ping":
						jsoResult = network.Ping(jsoCmd)
					case "storage_exist":
						jsoResult = storage.Exist(jsoCmd)
					case "storage_info":
						jsoResult = storage.Info(jsoCmd)
					case "storage_read":
						jsoResult = storage.Read(jsoCmd)
					case "storage_write":
						jsoResult = storage.Write(jsoCmd)
					case "storage_rewrite":
						jsoResult = storage.Rewrite(jsoCmd)
					case "storage_unlink":
						jsoResult = storage.Unlink(jsoCmd)
					}
				}
			}
		}
	}

	buffer, err := jsoResult.ToBytes()
	if err != nil {
		jsoData := jsons.ObjectNew(0)
		jsoData.PutNumber("int_status", 0)
		jsoData.PutString("txt_msg", fmt.Sprint(err))
		buffer, err := jsoData.ToBytes()
		if err == nil {
			w.Write(buffer)
		}
	} else {
		w.Write(buffer)
	}
}
