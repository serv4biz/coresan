package main

import (
	"fmt"
	"time"

	"gitlab.com/serv4biz/coresan/apps/server/global"
	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/jsons"
)

func DeepScan(pathDir string) error {
	arrFiles, err := files.ScanDir(pathDir)
	if err != nil {
		return err
	}

	for _, pathName := range arrFiles {
		pathFile := fmt.Sprint(pathDir, global.DS, pathName)
		if files.IsDir(pathFile) {
			err = DeepScan(pathFile)
			if err != nil {
				return err
			}
		} else {
			pathInfo := fmt.Sprint(pathDir, global.DS, "info.json")
			pathData := fmt.Sprint(pathDir, global.DS, "data.dat")

			if pathName == "info.json" {
				if !files.ExistFile(pathData) {
					err = files.DeleteFile(pathFile)
					if err != nil {
						return err
					}
				}
			}

			if pathName == "data.dat" {
				if !files.ExistFile(pathInfo) {
					err = files.DeleteFile(pathFile)
					if err != nil {
						return err
					}
				}
			}

			if pathName != "info.json" && pathName != "data.dat" {
				err = files.DeleteFile(pathFile)
				if err != nil {
					return err
				}
			}

			if files.ExistFile(pathInfo) {
				jsoInfo, err := jsons.ObjectFromFile(pathInfo)
				if err != nil {
					return err
				}

				if jsoInfo.Int("int_expire") > 0 {
					intAge := time.Now().Unix() - int64(jsoInfo.Int("int_stamp"))
					if int(intAge) >= jsoInfo.Int("int_expire") {
						err = files.DeleteFile(pathInfo)
						if err != nil {
							return err
						}

						err = files.DeleteFile(pathData)
						if err != nil {
							return err
						}
					}
				}
			}
		}
	}

	arrFiles, err = files.ScanDir(pathDir)
	if err != nil {
		return err
	}
	if len(arrFiles) == 0 {
		err = files.DeleteFile(pathDir)
		if err != nil {
			return err
		}
	}

	return nil
}
