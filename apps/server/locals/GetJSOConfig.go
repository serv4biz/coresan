package locals

import (
	"gitlab.com/serv4biz/coresan/apps/server/global"

	"gitlab.com/serv4biz/gfp/jsons"
)

// GetJSOConfig is get copy json object
func GetJSOConfig() (*jsons.Object, error) {
	global.MutexJSOConfig.Lock()
	defer global.MutexJSOConfig.Unlock()
	return global.JSOConfig.Clone()
}
