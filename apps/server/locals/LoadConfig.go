package locals

import (
	"fmt"

	"gitlab.com/serv4biz/coresan/apps/server/global"

	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/jsons"
)

// LoadConfig is load json config
func LoadConfig() (*jsons.Object, error) {
	pathfile := fmt.Sprint(global.AppDir, global.DS, "config.json")
	jsoConfig := jsons.ObjectNew(0)
	jsoConfig.PutString("txt_host", "localhost")
	jsoConfig.PutNumber("int_port", 8765)

	if files.ExistFile(pathfile) {
		var errConfig error
		jsoConfig, errConfig = jsons.ObjectFromFile(pathfile)
		if errConfig != nil {
			return nil, errConfig
		}
	}
	return jsoConfig, nil
}
