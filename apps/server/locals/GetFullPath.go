package locals

import (
	"fmt"
	"strings"
	"time"

	"github.com/oklog/ulid/v2"
	"gitlab.com/serv4biz/coresan/apps/server/global"
)

// GetFullPath is get fullpath
func GetFullPath(txtULID string) (string, error) {
	if strings.Contains(txtULID, "-") {
		arrPaths := strings.SplitN(strings.TrimSpace(strings.ToLower(txtULID)), "-", 3)
		return fmt.Sprint(global.NFSPath, global.DS, arrPaths[0], global.DS, arrPaths[1], global.DS, arrPaths[2]), nil
	}

	myulid, err := ulid.Parse(txtULID)
	if err != nil {
		return "", err
	}
	dt := time.Unix(int64(myulid.Time()/1000), 0)
	return fmt.Sprint(global.NFSPath, global.DS, fmt.Sprintf("%04d%02d%02d", dt.UTC().Year(), int(dt.UTC().Month()), dt.UTC().Day()), global.DS, strings.ToUpper(txtULID)), nil
}
