package main

import (
	"fmt"
	"io/fs"
	"net/http"
	"runtime"
	"time"

	"gitlab.com/serv4biz/coresan/apps/server/global"
	"gitlab.com/serv4biz/coresan/apps/server/locals"
	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/logs"
)

func main() {
	pathAppDir, err := files.GetAppDir()
	logs.Panic(err, true)
	global.AppDir = pathAppDir

	global.JSOConfig, err = locals.LoadConfig()
	logs.Panic(err, true)

	global.NFSPath = global.JSOConfig.String("txt_path_nfs")
	global.Username = global.JSOConfig.String("txt_username")
	global.Password = global.JSOConfig.String("txt_password")

	global.MaxRead = global.JSOConfig.Int("int_max_read")
	if global.MaxRead <= 0 {
		// Default max reader is 1024MB or 1GB
		global.MaxRead = 1024 * 1024 * 1024
	}

	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println(fmt.Sprint(global.AppName, " Version ", global.AppVersion))
	fmt.Println("Copyright © 2019 Serv4Biz Co.,Ltd. All Rights Reserved.")
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println(fmt.Sprint("Directory : ", global.AppDir))
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println("Loading configuration file.")
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println("")

	txtJSON, err := global.JSOConfig.ToString()
	logs.Panic(err, true)
	fmt.Println(txtJSON)

	fmt.Println("")
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	err = files.MakeDir(global.NFSPath, fs.ModePerm)
	logs.Panic(err, true)

	// Force GC to clear up
	go func() {
		for {
			<-time.After(time.Hour)
			runtime.GC()
		}
	}()

	// Remove data all expired from storage
	go func() {
		for {
			err := DeepScan(global.NFSPath)
			if err != nil {
				fmt.Println(err)
			}
			<-time.After(time.Hour)
		}
	}()

	intTime := global.JSOConfig.Int("int_timeout")
	if intTime <= 0 {
		intTime = 15
	}

	router := http.NewServeMux()
	router.HandleFunc("/", WorkHandler)

	appsrv := &http.Server{
		Addr:         fmt.Sprint(":", global.JSOConfig.Int("int_port")),
		Handler:      router,
		ReadTimeout:  time.Duration(intTime) * time.Second,
		WriteTimeout: time.Duration(intTime) * time.Second,
	}
	appsrv.ListenAndServe()
}
