#!/bin/bash

NAME_LINUX_AMD64="server.linux.amd64"
NAME_LINUX_ARM64="server.linux.arm64"

NAME_DARWIN_AMD64="server.darwin.amd64"
NAME_DARWIN_ARM64="server.darwin.arm64"

NAME_WINDOWS_AMD64="server.windows.amd64"
NAME_WINDOWS_ARM64="server.windows.arm64"

NAME_FREEBSD_AMD64="server.freebsd.amd64"
NAME_FREEBSD_ARM64="server.freebsd.arm64"

export GO111MODULE=auto
export CGO_ENABLED=0

# Linux
rm -f $NAME_LINUX_AMD64
export GOOS=linux
export GOARCH=amd64
go build -o $NAME_LINUX_AMD64 -ldflags="-s -w"

rm -f $NAME_LINUX_ARM64
export GOOS=linux
export GOARCH=arm64
go build -o $NAME_LINUX_ARM64 -ldflags="-s -w"

# Darwin
rm -f $NAME_DARWIN_AMD64
export GOOS=darwin
export GOARCH=amd64
go build -o $NAME_DARWIN_AMD64 -ldflags="-s -w"

rm -f $NAME_DARWIN_ARM64
export GOOS=darwin
export GOARCH=arm64
go build -o $NAME_DARWIN_ARM64 -ldflags="-s -w"

# Windows
rm -f $NAME_WINDOWS_AMD64
export GOOS=windows
export GOARCH=amd64
go build -o $NAME_WINDOWS_AMD64 -ldflags="-s -w"

rm -f $NAME_WINDOWS_ARM64
export GOOS=windows
export GOARCH=arm64
go build -o $NAME_WINDOWS_ARM64 -ldflags="-s -w"

# FreeBSD
rm -f $NAME_FREEBSD_AMD64
export GOOS=freebsd
export GOARCH=amd64
go build -o $NAME_FREEBSD_AMD64 -ldflags="-s -w"

rm -f $NAME_FREEBSD_ARM64
export GOOS=freebsd
export GOARCH=arm64
go build -o $NAME_FREEBSD_ARM64 -ldflags="-s -w"