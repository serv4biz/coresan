#!/bin/bash

RUNNAME="server.$1.$2"
PORT=$3

clear
./build.sh

kill -9 $(lsof -t -i:$PORT)
./$RUNNAME