package global

import (
	"sync"

	"gitlab.com/serv4biz/gfp/jsons"
)

// AppName is name of application
var AppName string = "CORESAN SERVER"

// AppVersion is version of application
var AppVersion string = "1.0.0"

var AppDir = "./"

// DS is split of path
var DS string = "/"

// MutexJSOConfig is mutex lock of JSOConfig
var MutexJSOConfig sync.RWMutex

// JSOConfig is config json object
var JSOConfig *jsons.Object

// Username is authen username
var Username = ""

// Password is authen password
var Password = ""

// NFSPath is base nfs path
var NFSPath = ""

// MaxRead is Max MultiPart of body in request
var MaxRead int = 0
