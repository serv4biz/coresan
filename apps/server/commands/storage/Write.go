package storage

import (
	"encoding/base64"
	"fmt"
	"io/fs"
	"strings"
	"time"

	"gitlab.com/serv4biz/coresan/apps/server/global"
	"gitlab.com/serv4biz/coresan/apps/server/locals"
	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/gfp/ulids"
)

// Write is write data to coresan
func Write(jsoCmd *jsons.Object) *jsons.Object {
	jsoResult := jsons.ObjectNew(0)
	jsoResult.PutNumber("int_status", 0)

	myULID, err := ulids.New()
	if err != nil {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return jsoResult
	}
	txtCSNID := myULID.String()
	txtFilename, ok := jsoCmd.GetString("txt_filename")
	if !ok {
		jsoResult.PutString("txt_msg", "no txt_filename found in json object")
		return jsoResult
	}
	intExpire, ok := jsoCmd.GetInt("int_expire")
	if !ok {
		jsoResult.PutString("txt_msg", "no int_expire found in json object")
		return jsoResult
	}
	txtData, ok := jsoCmd.GetString("txt_data")
	if !ok {
		jsoResult.PutString("txt_msg", "no txt_data found in json object")
		return jsoResult
	}

	txtExt := "dat"
	exts := strings.Split(txtFilename, ".")
	if len(exts) >= 2 {
		txtExt = strings.ToLower(exts[len(exts)-1])
	}

	txtFullpath, err := locals.GetFullPath(txtCSNID)
	if err != nil {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return jsoResult
	}
	txtInfopath := fmt.Sprint(txtFullpath, global.DS, "info.json")
	txtDatapath := fmt.Sprint(txtFullpath, global.DS, "data.dat")

	buffer, err := base64.StdEncoding.DecodeString(txtData)
	if err != nil {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return jsoResult
	}

	err = files.MakeDir(txtFullpath, fs.ModePerm)
	if err != nil {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return jsoResult
	}

	intSize, err := files.WriteFile(txtDatapath, buffer, fs.ModePerm)
	if err != nil {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return jsoResult
	}

	jsoInfo := jsons.ObjectNew(0)
	jsoInfo.PutString("txt_csnid", txtCSNID)
	jsoInfo.PutString("txt_name", txtFilename)
	jsoInfo.PutString("txt_ext", txtExt)
	jsoInfo.PutInt("int_stamp", int(time.Now().Unix()))
	jsoInfo.PutInt("int_expire", intExpire)
	jsoInfo.PutInt("int_size", intSize)

	_, errToFile := jsoInfo.ToFile(txtInfopath)
	if errToFile != nil {
		jsoResult.PutString("txt_msg", fmt.Sprint("can't export json to file [ ", errToFile, " ]"))
		return jsoResult
	}

	jsoResult.SetObject("jso_data", jsoInfo)
	jsoResult.PutNumber("int_status", 1)
	return jsoResult
}
