package storage

import (
	"encoding/base64"
	"fmt"
	"io/fs"
	"strings"
	"time"

	"gitlab.com/serv4biz/coresan/apps/server/global"
	"gitlab.com/serv4biz/coresan/apps/server/locals"
	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
)

// Rewrite is write data to coresan
func Rewrite(jsoCmd *jsons.Object) *jsons.Object {
	jsoResult := jsons.ObjectNew(0)
	jsoResult.PutNumber("int_status", 0)

	txtCSNID := strings.TrimSpace(jsoCmd.String("txt_csnid"))
	txtFilename := jsoCmd.String("txt_filename")
	intExpire := jsoCmd.Int("int_expire")
	txtData := jsoCmd.String("txt_data")
	txtExt := "dat"
	exts := strings.Split(txtFilename, ".")
	if len(exts) >= 2 {
		txtExt = strings.ToLower(exts[len(exts)-1])
	}

	txtFullpath, err := locals.GetFullPath(txtCSNID)
	if err != nil {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return jsoResult
	}
	txtInfopath := fmt.Sprint(txtFullpath, global.DS, "info.json")
	txtDatapath := fmt.Sprint(txtFullpath, global.DS, "data.dat")

	buffer, err := base64.StdEncoding.DecodeString(txtData)
	if err != nil {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return jsoResult
	}

	err = files.MakeDir(txtFullpath, fs.ModePerm)
	if err != nil {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return jsoResult
	}

	intSize, err := files.WriteFile(txtDatapath, buffer, fs.ModePerm)
	if err != nil {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return jsoResult
	}

	jsoInfo := jsons.ObjectNew(0)
	jsoInfo.PutString("txt_csnid", txtCSNID)
	jsoInfo.PutString("txt_name", txtFilename)
	jsoInfo.PutString("txt_ext", txtExt)
	jsoInfo.PutInt("int_stamp", int(time.Now().Unix()))
	jsoInfo.PutInt("int_expire", intExpire)
	jsoInfo.PutInt("int_size", intSize)

	_, err = jsoInfo.ToFile(txtInfopath)
	if err != nil {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return jsoResult
	}

	jsoResult.PutObject("jso_data", jsoInfo)
	jsoResult.PutNumber("int_status", 1)
	return jsoResult
}
