package storage

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/serv4biz/coresan/apps/server/global"
	"gitlab.com/serv4biz/coresan/apps/server/locals"
	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
)

// Unlink is delete file from coresan
func Unlink(jsoCmd *jsons.Object) *jsons.Object {
	jsoResult := jsons.ObjectNew(0)
	jsoResult.PutNumber("int_status", 0)

	txtCSNID := strings.TrimSpace(jsoCmd.String("txt_csnid"))
	txtFullpath, err := locals.GetFullPath(txtCSNID)
	if err != nil {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return jsoResult
	}
	txtInfopath := fmt.Sprint(txtFullpath, global.DS, "info.json")
	txtDatapath := fmt.Sprint(txtFullpath, global.DS, "data.dat")
	if strings.TrimSpace(txtCSNID) == "" || !files.ExistFile(txtInfopath) || !files.ExistFile(txtDatapath) {
		jsoResult.PutString("txt_msg", logs.Message(errors.New("not exist file")))
		return jsoResult
	}

	err = files.DeleteFile(txtFullpath)
	if err != nil {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return jsoResult
	}

	jsoResult.PutNumber("int_status", 1)
	return jsoResult
}
