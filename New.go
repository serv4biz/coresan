package coresan

import (
	"gitlab.com/serv4biz/gfp/jsons"
)

// New is begin HScaleDB object same Factory
func New(jsoConfigHost *jsons.Object) (*CORESAN, error) {
	return Factory(jsoConfigHost)
}
