module gitlab.com/serv4biz/coresan

go 1.23

replace gitlab.com/serv4biz/coresan => ./

require (
	github.com/oklog/ulid/v2 v2.1.0
	gitlab.com/serv4biz/gfp v1.2.6
)
