package coresan

import (
	"errors"

	"gitlab.com/serv4biz/coresan/networks"
	"gitlab.com/serv4biz/gfp/jsons"
)

// DataNodeListing is list all datanode
func (me *CORESAN) NodeList() (*jsons.Array, error) {
	jsoCmd := jsons.ObjectNew(0)
	jsoCmd.PutObject("jso_authen", me.AuthenInfo())
	jsoCmd.PutString("txt_command", "node_list")

	jsoReq := networks.Request(me.JSOConfigHost, jsoCmd)
	if jsoReq.Int("int_status") > 0 {
		jsoData := jsoReq.Object("jso_data")
		return jsoData.Array("jsa_list"), nil
	}
	return nil, errors.New(jsoReq.String("txt_msg"))
}
